# Git LFS Usage Within a GitLab CI Pipeline Context

## Problem Definition / Question Under Exploration

Git LFS can add a lot of burden on a pipeline performance (i.e. time to
complete) and can consume an order of magnitude more resources (i.e.
hard disk space) for the GitLab Runner grabbing the job. What can be
done to tame this situation?

## Goal

Reduce footprint of a Git repository configured with Git LFS when said
Git repository is checked out in the *Getting source from Git repository*
step of a job.

## Measurement Strategy

How can we measure the impact of an exploration idea?

### Controlling the Environment

Avoid going down the rabbit hole on side quests by NOT verify and
calibrate every parts of the environment used for this exploration. This
experiment has no intention of being academically published or
anything. Let's note assumptions under which this experiment will
be conduct.

**Assumption 0:** Controlling the loaded kernel version and making sure
that the same kernel is loaded in a containerized fashion when collecting
data is **sufficient**.

**Assumption 1:** Controlling the file system being used and making
sure that the same file system configuration is used when collecting
data is **sufficient**.

**Assumption 2:** Controlling the versions of tools being used to measure,
and making sure that the same versions are used when collecting data
is **sufficient**.

### Measurement Tools

- [du](https://www.gnu.org/software/coreutils/manual/html_node/du-invocation.html)
- [df](https://www.gnu.org/software/coreutils/manual/html_node/df-invocation.html)
- [git-sizer](https://github.com/github/git-sizer/)

# Observations

## Base Line

We see disk usage having everything.

`git-sizer` has very low disk usage since blobs are tracked by `git-lfs`.

## Limiting Git Clone Depth

Limiting Git clone depth (shallow clone) does not have significative
impact on disk usage: `-200` compared to the base line disk usage.

Important to note that the shallow clone voids the report of `git-sizer`
since a full clone is required for `git-sizer` to work.

**Hypothesis:** Since blobs are configured to be tracked by `git-lfs`,
there is not a signicative impact when cloning a minimal depth.

**Hypothesis:** There is not a large history in the repository at the
moment of making this observation, therefore limiting the history to be
cloned by a job does not have a significative impact.

## Disabling Git LFS Smudge Process

Not surprisingly, this is a real gold mine in order to minimize disk
usage footprint of a repository using `git-lfs`. A significative impact
on disk usage: `-2101068` compared to the base line disk usage.

# Appendices

## A. Data Report

|               Job              | Disk Usage (du) |
| ------------------------------ | --------------- |
| Base line                      | [2101628](https://gitlab.com/ssirois/git-lfs-and-pipelines/-/jobs/6996757181#L54) |
| Limit Git Clone Depth          | [2101428](https://gitlab.com/ssirois/git-lfs-and-pipelines/-/jobs/6996757182#L54) |
| Disable Git LFS Smudge Process | [560](https://gitlab.com/ssirois/git-lfs-and-pipelines/-/jobs/6996757183#L54) |

## B. Creating a Big Random File

```terminal
$ touch ./assets/1G-file.random
$ du ./assets/1G-file.random
0       ./assets/1G-file.random
$ shred --iterations=1 --size=1G ./assets/1G-file.random
$ du ./assets/1G-file.random
1048580 ./assets/1G-file.random
```

## C. Resources

- [git-lfs](https://git-lfs.com/)
- [git-sizer](https://github.com/github/git-sizer)
- [GitLab CI Pipeline Efficiency](https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html)
- [GitLab CI Shallow Cloning](https://docs.gitlab.com/ee/user/project/repository/monorepos/index.html#shallow-cloning)
- [GitLab CI Git Strategy](https://docs.gitlab.com/ee/user/project/repository/monorepos/index.html#git-strategy)

> Git LFS is a system for managing and versioning large files in association
> with a Git repository. Instead of storing the large files within the
> Git repository as blobs, Git LFS stores special "pointer files" in
> the repository, while storing the actual file contents on a Git LFS
> server. The contents of the large file are downloaded automatically
> when needed, for example when a Git branch containing the large file is
> checked out.

— man 1 git-lfs
