#!/usr/bin/env sh
set -e

cd "$(dirname "$0")/../"

set -x
  docker compose run \
    --rm --service-ports \
    "dev-env" \
    "${@}"
{ set +x; } >/dev/null 2>&1
