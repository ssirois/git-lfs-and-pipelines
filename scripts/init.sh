#!/usr/bin/env sh
set -e

cd "$(dirname "$0")/../"

if [ ! -f ./.env ];
then
  echo ".env could not be found."
  echo "Creating .env file with default values..."
  cat <<- EOF > ./.env
	HOST_UID=$(id -u)
	HOST_GID=$(id -u)
EOF
  echo "...done."
fi

set -x
  docker compose build
{ set +x; } >/dev/null 2>&1
