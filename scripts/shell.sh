#!/usr/bin/env sh
set -e

cd "$(dirname "$0")/../"

set -x
  docker compose run \
    --rm --service-ports \
    "dev-env" \
    "/usr/bin/sh"
{ set +x; } >/dev/null 2>&1
