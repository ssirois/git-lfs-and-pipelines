#!/usr/bin/env sh
#
# A script to measure the space used by the git repository.

PROJECT_ROOT="$(git rev-parse --show-toplevel)"

DU_BIN=$(command -v du)

echo "Measuring ${PROJECT_ROOT}..."
  echo "Disk Usage (du)..."
    du -s "${PROJECT_ROOT}"
  echo

  echo "Git stats (git-sizer)..."
    git-sizer --verbose
  echo
echo

exit 0
