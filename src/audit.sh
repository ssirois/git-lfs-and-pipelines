#!/usr/bin/env sh
#
# A script to audit and report on what is available in the controlled
# environment.

#######################################
# Get informations on a binary.
#
# Check if a binary is available on the system and outputs informations
# about it. See "Outputs".
#
# Globals:
#   None
# Arguments:
#   Binary name, a string.
#
#   Version option, a string.
#   The string must represent the exact option that must be use to
#   receive the version for the binary; e.g. "-v", "-V", "--version", etc.
# Returns:
#   0 when binary is found, non-zero otherwise.
# Outputs:
#   - binary path
#   - binary one-line description
#   - binary version
#   - binary hash
#######################################
audit_binary() {
  local bin_name=${1}
  local version_option=${2}
  echo "Checking for binary <${bin_name}>..."
  local bin; bin=$(command -v ${bin_name})
  if [ $? = 0 ];
  then
    echo "  Found <${bin_name}> utility at: ${bin}"
    local bin_doc; bin_doc=$(whatis --section=1 "${bin}" 2>/dev/null)
    if [ $? = 0 ];
    then
      echo "  ${bin_doc}"
    else
      echo "  ${bin_name} (1)        - no documentation found"
    fi
    echo "  Version: $(${bin} ${version_option} | head -n1)"
    echo "  $(openssl dgst -sha256 ${bin})"

    return 0
  else
    echo "  Cannot find <${bin_name}> binary." >&2

    return 1
  fi
}

echo "Audit kernel..."
  echo "  $(uname -a)"
echo

echo "Audit file system..."
  df --print-type $(pwd)
echo

echo "Audit measuring tools..."
  audit_binary "openssl" "version"
  openssl_ok=$?
  echo
  if [ $openssl_ok != 0 ];
  then
    echo "Fatal error:"
    echo "  problem auditing openssl and openssl is mandatory for the"
    echo "  rest of the audit."
    echo
    echo "Aborting."
    exit 1
  fi

  audit_binary "df" "--version"
  echo

  audit_binary "du" "--version"
  echo

  audit_binary "git" "--version"
  echo

  audit_binary "git-sizer" "--version"
echo

exit 0
